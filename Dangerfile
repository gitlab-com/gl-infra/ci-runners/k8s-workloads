# frozen_string_literal: true

MISSING_DESCRIPTION_MESSAGE = <<~END_OF_MESSAGE
  Please provide a merge request description.
END_OF_MESSAGE

SKIPPING_VALIDATION_MESSAGE = <<~END_OF_MESSAGE
  Skipping validation of merge request description because of ~backstage label.
END_OF_MESSAGE

MISSING_ISSUE_LINK_MESSAGE = <<~END_OF_MESSAGE
  **Please provide an issue link in the merge request description to the gitlab.com issue related to this change.**

  For version bumps, typos and other small changes, or in time-limited situations (eg, during an incident),
  add the label ~backstage to excempt this change from this requirement.
END_OF_MESSAGE

def validate_mr
  # The cookbook-publisher user is automatically creating MR's that are backstage in nature, so skip those MR's.
  return if gitlab.mr_author == 'cookbook-publisher'

  validate_mr_description
end

def validate_mr_description
  fail MISSING_DESCRIPTION_MESSAGE if gitlab.mr_body.empty?

  # `backstage` label describes smaller changes like fixing typos and docs, we can safely skip those MR's.
  if gitlab.mr_labels.include? 'backstage'
    message SKIPPING_VALIDATION_MESSAGE
    return
  end

  fail MISSING_ISSUE_LINK_MESSAGE unless gitlab.mr_body.match?(%r{https://gitlab.com/[\w+-/]+/issues/\d+})
end

validate_mr
