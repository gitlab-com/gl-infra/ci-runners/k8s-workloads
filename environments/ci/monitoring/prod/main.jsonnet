local monitoring = import 'services/ci-runners/monitoring/monitoring.libsonnet';

local default = {
  initializeOnly: false,  // Set to false after applying CRDs first
  environment: 'ci',
  additionalScrapeConfigs: {
    project: 'gitlab-ci-155816',
    runnerManagers: [
      { shard: 'private', zone: 'us-east1-c' },
      { shard: 'private', zone: 'us-east1-d' },
      { shard: 'shared-gitlab-org', zone: 'us-east1-c' },
      { shard: 'shared-gitlab-org', zone: 'us-east1-d' },
    ],
    ephemeralRunners: [
      { shard: 'private', zone: 'us-east1-c' },
      { shard: 'private', zone: 'us-east1-d' },
      { shard: 'shared-gitlab-org', zone: 'us-east1-c' },
      { shard: 'shared-gitlab-org', zone: 'us-east1-d' },
    ],
  },
  gitlabMonitoringLabels+: {
    stage: 'cny',
  },
  thanosStorageServiceAccount: 'ci-thanos-storage@gitlab-ci-155816.iam.gserviceaccount.com',
};

local loadBalancerIPMixin(loadBalancerIP) =
  if loadBalancerIP != '' then
    {
      loadBalancer+: {
        IP: loadBalancerIP,
      },
    }
  else
    {}
;

local oAuth2Mixin(oAuth2) =
  if oAuth2 != null then
    assert std.objectHas(oAuth2, 'googleClientID');
    assert std.objectHas(oAuth2, 'googleClientSecret');
    assert std.objectHas(oAuth2, 'secret');
    {
      forwardAuth+: {
        secret+: {
          data+: oAuth2,
        },
      },
    }
  else
    {}
;

function(params={}, loadBalancerIP='', oAuth2=null)
  monitoring(
    default +
    params +
    loadBalancerIPMixin(loadBalancerIP) +
    oAuth2Mixin(oAuth2)
  )
