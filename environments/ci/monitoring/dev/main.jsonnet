local prod = import '../prod/main.jsonnet';
local k = import 'github.com/grafana/jsonnet-libs/ksonnet-util/kausal.libsonnet';
local overrides = import 'overrides.jsonnet';

local pvc = k.core.v1.persistentVolumeClaim;

local config = {
  initializeOnly: false,  // Set to false after applying CRDs first
  kubePromOverrides: {
    prometheus+:: {
      prometheus+: {
        spec+: {
          storage+: {
            volumeClaimTemplate+: pvc.spec.withStorageClassName('local-path'),
          },
        },
      },
    },
  },
  thanosStoreOverrides: {
    volumeClaimTemplate+: pvc.spec.withStorageClassName('local-path'),
  },
  thanosCompactOverrides: {
    volumeClaimTemplate+: pvc.spec.withStorageClassName('local-path'),
  },
  traefikOverrides: {
    dashboard+: {
      insecure: true,
    },
    acme+: {
      storage+: {
        pvc+: {
          storageClassName: 'local-path',
        },
      },
    },
  },
};

function(gceCredentials='', oAuth2=null)
  prod(config, '127.0.0.11', oAuth2) +
  if !config.initializeOnly then
    overrides.nodeExporterForK3D +
    overrides.gceCredentials(gceCredentials)
  else
    {}
