.PHONY: tk-fmt
tk-fmt:
	@tk fmt environments/ lib/

.PHONY: check-tk-fmt
check-tk-fmt:
	@$(MAKE) tk-fmt
	@git --no-pager diff --compact-summary --exit-code -- \
		$(shell git ls-files | grep -e '.jsonnet' -e '.libsonnet') && \
		echo "Formatting up-to-date!"

TANKA_ENV ?= unknown

gceCredentialsFile := .gitlab-ci.key.json
gceCredentials ?= unknown
ifneq (,$(wildcard $(gceCredentialsFile)))
gceCredentials := $(shell cat $(gceCredentialsFile) | base64 -w 0)
endif

oAuth2File := .oauth2.key.json
oAuth2 ?= null
ifneq (,$(wildcard $(oAuth2File)))
oAuth2 := $(shell cat $(oAuth2File))
endif

tkCommonFlags := --tla-code 'gceCredentials="$(gceCredentials)"' \
		 --tla-code oAuth2='$(oAuth2) + {secret: "$(shell dd if=/dev/random count=1 bs=128 | base64 -w 0)"}'

.PHONY: tanka-manifests-eval
tanka-manifests-eval:
	@tk eval environments/$(TANKA_ENV) $(tkCommonFlags)

.PHONY: tanka-manifests-export
tanka-manifests-export:
	@rm -rf build/
	@tk export build/ environments/$(TANKA_ENV) \
		--format '{{.apiVersion}}.{{.kind}}-{{if .metadata.namespace}}{{.metadata.namespace}}-{{end}}{{.metadata.name}}' \
		$(tkCommonFlags)

.PHONY: tanka-manifests-diff
tanka-manifests-diff:
	@tk diff --with-prune environments/$(TANKA_ENV) $(tkCommonFlags)

.PHONY: tanka-manifests-apply
tanka-manifests-apply:
	@tk apply environments/$(TANKA_ENV) $(tkCommonFlags)
	@tk prune environments/$(TANKA_ENV) $(tkCommonFlags)

.PHONY: tanka-manifests-delete
tanka-manifests-delete:
	@tk delete environments/$(TANKA_ENV) $(tkCommonFlags)
