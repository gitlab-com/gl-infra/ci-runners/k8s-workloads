include Makefile.*.mk

.PHONY: dependencies-install
dependencies-install:
	@jb install

.PHONY: build-gitlab-ci-yaml
build-gitlab-ci-yaml:
	@jsonnet \
		-S \
		--output-file .gitlab-ci.yml \
		.gitlab/ci/main.jsonnet

.PHONY: check-gitlab-ci-yml
check-gitlab-ci-yml:
	@$(MAKE) build-gitlab-ci-yaml
	@git --no-pager diff --compact-summary --exit-code -- .gitlab-ci.yml && \
		echo ".gitlab-ci.yml is up-to-date!"

.PHONY: jsonnet-fmt
jsonnet-fmt:
	@for file in $(shell git ls-files | grep -e '.jsonnet' -e '.libsonnet'); do jsonnetfmt -i "$${file}"; done

.PHONY: check-jsonnet-fmt
check-jsonnet-fmt:
	@$(MAKE) jsonnet-fmt
	@git --no-pager diff --compact-summary --exit-code -- \
		$(shell git ls-files | grep -e '.jsonnet' -e '.libsonnet') && \
		echo "Formatting up-to-date!"
