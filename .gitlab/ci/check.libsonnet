local job = import 'lib/job.libsonnet';
local rules = import 'lib/rules.libsonnet';

local woodhouseImage = 'registry.gitlab.com/gitlab-com/gl-infra/woodhouse:latest';

{
  // Uses Woodhouse (https://gitlab.com/gitlab-com/gl-infra/woodhouse) notification subcommand to add a MR comment
  // on the canonical repository.
  // See CONTRIBUTE.md#project-workflow for more information.
  notify_mirror_source:
    job.withStage('check') +
    job.withAllowFailure(true) +
    job.withRules(rules.conditions.ifMirror) +
    job.withNeeds() +
    job.withImage(woodhouseImage) +
    job.withScript('woodhouse gitlab notify-mirrored-mr'),

  check_remote:
    job.withStage('check') +
    job.withRules(rules.conditions.ifCanonical) +
    job.withNeeds() +
    job.withImage(woodhouseImage) +
    job.withScript('woodhouse gitlab follow-remote-pipeline --gitlab-api-base-url="https://ops.gitlab.net/api/v4" --gitlab-api-token="$OPS_API_TOKEN"'),
}
