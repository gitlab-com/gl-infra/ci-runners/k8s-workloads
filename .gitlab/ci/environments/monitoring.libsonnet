local kube = import '../lib/kube.libsonnet';

{
  local s = self,

  changes:
    [
      '.gitlab/ci/environments/monitoring.libsonnet',
      'lib/services/ci-runners/monitoring/*',
    ],
  tankaFlags:
    [
      '--tla-code "loadBalancerIP=\'${LB_IP_ADDRESS}\'"',
      '--tla-code "oAuth2={googleClientID: \'${OAUTH2_GOOGLE_CLIENT_ID}\', googleClientSecret: \'${OAUTH2_GOOGLE_CLIENT_SECRET}\', secret: \'${FORWARD_AUTH_SECRET}\'}"',
    ],
  preScript(environment):
    [
      "export LB_IP_ADDRESS=$(host -t a monitoring-lb.%s.ci-runners.gitlab.net | sed -r 's/.* has address //')" % environment,
      'export FORWARD_AUTH_SECRET=$(dd if=/dev/random count=1 bs=128 | base64 -w 0)',
    ],

  planDev(environment): {
    ['%s monitoring plan dev' % environment]:
      kube.plan.dev(
        environment,
        'monitoring',
        tankaFlags=s.tankaFlags,
        changes=s.changes,
        preScript=s.preScript(environment),
      ),
  },
  planProd(environment): {
    ['%s monitoring plan prod' % environment]:
      kube.plan.prod(
        environment,
        'monitoring',
        tankaFlags=s.tankaFlags,
        changes=s.changes,
        preScript=s.preScript(environment),
      ),
  },
  applyProd(environment): {
    ['%s monitoring apply prod' % environment]:
      kube.apply.prod(
        environment,
        'monitoring',
        tankaFlags=s.tankaFlags,
        changes=s.changes,
        preScript=s.preScript(environment),
      ),
  },
}
