local k = import 'github.com/grafana/jsonnet-libs/ksonnet-util/kausal.libsonnet';

local secret = k.core.v1.secret;

local ascSecretName = 'additional-scrape-config';
local ascSecretKey = 'additional-scrape-config.yml';

local gceServiceDiscovery = function(project, zone, port, type, shard, refresh_interval='60s') {
  project: project,
  zone: zone,
  port: port,
  filter: '(labels.gl_resource_type="%s") AND (labels.shard="%s")' % [type, shard],
  refresh_interval: refresh_interval,
};

local jobs = function(name, config) {
  local instanceFQDNReplacement = '${1}.c.%s.internal' % config.project,

  job_name: name,
  gce_sd_configs: [
    cfg
    for cfg in config.sdConfigs
  ],
  relabel_configs:
    [
      // set the instance label from GCE instance name
      {
        target_label: 'instance',
        source_labels: [
          '__meta_gce_instance_name',
        ],
        replacement: instanceFQDNReplacement,
      },
      // set the fqdn label from GCE instance name (fqdn is used by some dashboards and alerts)
      {
        target_label: 'fqdn',
        source_labels: [
          '__meta_gce_instance_name',
        ],
        replacement: instanceFQDNReplacement,
      },
      // set the zone label from GCE zone
      {
        target_label: 'zone',
        source_labels: [
          '__meta_gce_zone',
        ],
        regex: '.*/([^/]+)$',
        replacement: '$1',
      },
      // set the shard label from GCE runner_manager_group label
      {
        target_label: 'shard',
        source_labels: [
          '__meta_gce_label_shard',
        ],
      },
    ] +
    config.relabelConfig,

  metric_relabel_configs:
    [
      {
        regex: '(env|environment|stage|tier|ci_environment)',
        replacement: 'exported_${1}',
        action: 'labelmap',
      },
      {
        regex: '(env|environment|stage|tier|ci_environment)',
        action: 'labeldrop',
      },
    ] +
    config.metricRelabelConfig,
};

local deploymentRelabel = function(sourceLabel) [
  {
    target_label: 'deployment',
    source_labels: [
      '__meta_gce_label_%s' % sourceLabel,
    ],
  },
];

local factory = function(config)
  {
    local s = self,
    local project = config.additionalScrapeConfigs.project,

    _config:: {
      'runners-manager': {
        project: project,
        sdConfigs: [],
        relabelConfig:
          deploymentRelabel('deployment') +
          [
            { target_label: 'type', replacement: config.gitlabMonitoringLabels.type },
          ],
        metricRelabelConfig: [],
      },
      node: {
        project: project,
        sdConfigs: [],
        relabelConfig:
          deploymentRelabel('deployment') +
          [
            { target_label: 'type', replacement: config.gitlabMonitoringLabels.type },
          ],
        metricRelabelConfig: [],
      },
      'runner-ephemeral-vms': {
        project: project,
        sdConfigs: [],
        relabelConfig:
          deploymentRelabel('runner_manager_deployment') +
          [
            { target_label: 'type', replacement: 'ci-runners-ephemeral-vms' },
          ],
        metricRelabelConfig: [
          {
            action: 'drop',
            source_labels: [
              '__name__',
            ],
            regex: 'node_(scrape_collector_.*|cpu_guest_seconds_total|memory_(Active|Huge|Inactive|NFS_Unstable).*)',
          },
        ],
      },
    },

    addRunnerManagerScrapes: function(definitions)
      s {
        _config+:: {
          'runners-manager'+: {
            sdConfigs: [
              gceServiceDiscovery(project, d.zone, 9402, 'ci_manager', d.shard)
              for d in definitions
            ],
          },
          node+: {
            sdConfigs: [
              gceServiceDiscovery(project, d.zone, 9100, 'ci_manager', d.shard)
              for d in definitions
            ],
          },
        },
      },
    addEphemeralRunnersScrapes: function(definitions)
      s {
        _config+:: {
          'runner-ephemeral-vms'+: {
            sdConfigs: [
              gceServiceDiscovery(project, d.zone, 9100, 'ci_ephemeral', d.shard, refresh_interval='30s')
              for d in definitions
            ],
          },
        },
      },
    factorize: function()
      {
        'additional-scrape-configs-secret': secret.new(ascSecretName, {
          [ascSecretKey]: std.base64(std.manifestYamlDoc([
            jobs(name, s._config[name])
            for name in std.objectFields(s._config)
          ])),
        }),
        'prometheus-prometheus'+: {
          spec+: {
            additionalScrapeConfigs: {
              name: ascSecretName,
              key: ascSecretKey,
            },
          },
        },
      },
  };

local default = {
  additionalScrapeConfigs: {
    project: error 'must provide project',
    runnerManagers: [],
    ephemeralRunners: [],
  },
  gitlabMonitoringLabels: error 'must provide gitlabMonitoringLabels',
};

function(params)
  if params == {} then
    {}
  else
    local config = default + params;
    assert std.isObject(config);

    factory(config).
      addRunnerManagerScrapes(config.additionalScrapeConfigs.runnerManagers).
      addEphemeralRunnersScrapes(config.additionalScrapeConfigs.ephemeralRunners).
      factorize()
