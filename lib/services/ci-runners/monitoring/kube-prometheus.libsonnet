local k = import 'github.com/grafana/jsonnet-libs/ksonnet-util/kausal.libsonnet';

local pvc = k.core.v1.persistentVolumeClaim;

function(config)
  assert std.isObject(config);

  local staticOverrides =
    {
      values+:: {
        common+: {
          namespace: config.namespace,
          versions+: {
            prometheusOperator: '0.50.0',
          },
        },
        nodeExporter+: {
          name: 'gke-node',
        },
        prometheus+: {
          resources: {
            requests: {
              memory: '4Gi',
              cpu: 1,
            },
            limits: {
              memory: '6Gi',
              cpu: 3,
            },
          },
          externalLabels: config.externalLabels,
          thanos: {
            version: config.thanos.version,
            image: '%s:v%s' % [config.thanos.image, config.thanos.version],
            logFormat: config.thanos.log.format,
            logLevel: config.thanos.log.level,
            objectStorageConfig: {
              name: config.thanos.objectStorage.name,
              key: config.thanos.objectStorage.key,
            },
          },
        },
      },
      prometheus+:: {
        prometheus+: {
          spec+: {
            retention: config.prometheus.retention,
            storage: {
              volumeClaimTemplate: std.prune(
                pvc.new('deleteme') +
                pvc.metadata.withName(null) +
                pvc.spec.withAccessModes(['ReadWriteOnce']) +
                pvc.spec.withStorageClassName('premium-rwo') +
                pvc.spec.resources.withRequests({
                  storage: config.prometheus.storage,
                })
              ),
            },
          },
        },
      },
    };

  local kp =
    (import 'kube-prometheus/main.libsonnet') +
    staticOverrides +
    config.kubePromOverrides;

  // Custom Resource Definitions
  {
    ['setup/prometheus-operator-' + name]: kp.prometheusOperator[name]
    for name in std.filter((function(name) kp.prometheusOperator[name].kind == 'CustomResourceDefinition'), std.objectFields(kp.prometheusOperator))
  } +
  // Namespace
  {
    ['setup/namespace-' + name]: kp.kubePrometheus[name]
    for name in std.filter((function(name) name == 'namespace'), std.objectFields(kp.kubePrometheus))
  } +
  if !config.initializeOnly then
    // Kube Prometheus basics
    {
      ['setup/kube-prometheus-' + name]: kp.kubePrometheus[name]
      for name in std.filter((function(name) name != 'namespace'), std.objectFields(kp.kubePrometheus))
    } +
    // Prometheus Operator
    {
      ['setup/prometheus-operator-' + name]: kp.prometheusOperator[name]
      for name in std.filter((function(name) kp.prometheusOperator[name].kind != 'CustomResourceDefinition' && name != 'serviceMonitor'), std.objectFields(kp.prometheusOperator))
    } +
    {
      'setup/prometheus-operator-serviceMonitor': config._addCommonRelabelConfigs(kp.prometheusOperator.serviceMonitor),
    } +
    // Node exporter
    {
      ['node-exporter-' + name]: kp.nodeExporter[name]
      for name in std.filter((function(name) name != 'serviceMonitor'), std.objectFields(kp.nodeExporter))
    } +
    {
      'node-exporter-serviceMonitor': config._addCommonRelabelConfigs(kp.nodeExporter.serviceMonitor),
    } +
    // Kube State metrics
    {
      ['kube-state-metrics-' + name]: kp.kubeStateMetrics[name]
      for name in std.filter((function(name) name != 'serviceMonitor'), std.objectFields(kp.kubeStateMetrics))
    } +
    {
      'kube-state-metrics-serviceMonitor': config._addCommonRelabelConfigs(kp.kubeStateMetrics.serviceMonitor),
    } +
    // operated Prometheus
    {
      ['prometheus-' + name]: kp.prometheus[name]
      for name in std.filter((function(name) name != 'serviceMonitorThanosSidecar' && name != 'serviceMonitor'), std.objectFields(kp.prometheus))
    } +
    {
      'prometheus-serviceMonitor': config._addCommonRelabelConfigs(kp.prometheus.serviceMonitor),
      'prometheus-serviceMonitorThanosSidecar': config._addCommonRelabelConfigs(kp.prometheus.serviceMonitorThanosSidecar),
    }
  else
    {}
