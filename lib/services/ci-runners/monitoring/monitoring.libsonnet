local asc = import './additional-scrape-config.libsonnet';
local common = import './common.libsonnet';
local kubePrometheus = import './kube-prometheus.libsonnet';
local kubeThanos = import './kube-thanos.libsonnet';
local rules = import './rules.libsonnet';
local traefik = import './traefik.libsonnet';

local default = {
  local s = self,
  initializeOnly: true,
  environment: error 'must provide environment',
  namespace: 'monitoring',
  prometheus: {
    retention: '2d',
    storage: '100Gi',
  },
  thanos: {
    version: '0.20.1',
    image: 'quay.io/thanos/thanos',
    log: {
      format: 'json',
      level: 'warn',
    },
    objectStorage: {
      name: 'thanos-object-storage-config-secret',
      key: 'object-storage.yml',
      bucket: '%s-thanos-storage' % s.environment,
    },
    store: {
      replicas: 1,
      storage: '50Gi',
    },
    compact: {
      storage: '50Gi',
    },
  },
  thanosStorageServiceAccount: null,
  kubePromOverrides: {},
  thanosStoreOverrides: {},
  thanosCompactOverrides: {},
  traefikOverrides: {},
  additionalScrapeConfigs: {},
  tcpIngresses: {
    'thanos-sidecar': {
      entrypoint: 'thanos-sidecar',
      service: 'prometheus-k8s',
      port: 10901,
      targetPort: 10901,
    },
    'thanos-store': {
      entrypoint: 'thanos-store',
      service: 'thanos-store',
      port: 10903,
      targetPort: 10901,
    },
  },
  loadBalancer: {
    IP: error 'must provide loadBalancer.IP',
    sourceRanges: [
      '35.227.7.110/32',  // ops GKE Cloud NAT
      '34.75.8.251/32',  // ops GKE Cloud NAT
      '35.231.28.201/32',  // bastion-01-inf-ci; temporary
    ],
  },
  gitlabMonitoringLabels: {
    // Not assigning directly to externalLabels as `type` must be added manually to service monitors
    // and additional config, as we need to distinguish `ci-runners` and `ci-runners-ephemeral-vms`.
    // And for Thanos the external labels have always priority and are added to the metric - in case of
    // conflict with existing label in the metric - the external label sourced one gets the priority.
    env: 'gprd',
    environment: 'gprd',
    stage: 'main',
    tier: 'runners',
    type: 'ci-runners',
    ci_environment: s.environment,
  },
  externalLabels:
    {
      [labelName]: s.gitlabMonitoringLabels[labelName]
      for labelName in std.filter((function(name) name != 'type'), std.objectFields(s.gitlabMonitoringLabels))
    },

  // _addCommonRelabelConfigs
  // adds the `type` label to PrometheusServiceMonitor manifests
  _addCommonRelabelConfigs:
    function(serviceMonitor)
      assert std.isObject(serviceMonitor);
      serviceMonitor {
        spec+: {
          endpoints:
            [
              endpoint {
                relabelings+: [
                  {
                    targetLabel: 'type',
                    replacement: s.gitlabMonitoringLabels.type,
                  },
                ],
              }
              for endpoint in serviceMonitor.spec.endpoints
            ],
        },
      },
};

function(params)
  local config = default + params;
  assert std.isObject(config);

  local addGcpServiceAccountAnnotation(name) =
    if config.thanosStorageServiceAccount != null then
      {
        ['%s-serviceAccount' % name]+: {
          metadata+: {
            annotations+: {
              'iam.gke.io/gcp-service-account': config.thanosStorageServiceAccount,
            },
          },
        },
      }
    else
      {};

  kubePrometheus(config) +
  addGcpServiceAccountAnnotation('prometheus') +
  traefik(config) +
  if !config.initializeOnly then
    rules(config) +
    kubeThanos(config) +
    addGcpServiceAccountAnnotation('thanos-compact') +
    addGcpServiceAccountAnnotation('thanos-store') +
    common(config) +
    asc(config)
  else
    {}
