local k = import 'github.com/grafana/jsonnet-libs/ksonnet-util/kausal.libsonnet';

local pvc = k.core.v1.persistentVolumeClaim;

local store = function(config)
  assert std.isObject(config);

  local staticOverrides = {
    namespace: config.namespace,
    replicas: config.thanos.store.replicas,
    version: config.thanos.version,
    image: '%s:v%s' % [config.thanos.image, config.thanos.version],
    logFormat: config.thanos.log.format,
    logLevel: config.thanos.log.level,
    objectStorageConfig: {
      name: config.thanos.objectStorage.name,
      key: config.thanos.objectStorage.key,
    },
    serviceMonitor: true,
    volumeClaimTemplate: std.prune(
      pvc.new('data') +
      pvc.metadata.withName(null) +
      pvc.spec.withAccessModes(['ReadWriteOnce']) +
      pvc.spec.withStorageClassName('premium-rwo') +
      pvc.spec.resources.withRequests({
        storage: config.thanos.store.storage,
      })
    ),
  };

  (import 'github.com/thanos-io/kube-thanos/jsonnet/kube-thanos/kube-thanos-store.libsonnet')(
    staticOverrides +
    config.thanosStoreOverrides
  );

local compact = function(config)
  assert std.isObject(config);

  local staticOverrides = {
    namespace: config.namespace,
    version: config.thanos.version,
    image: '%s:v%s' % [config.thanos.image, config.thanos.version],
    logFormat: config.thanos.log.format,
    logLevel: config.thanos.log.level,
    replicas: 1,
    objectStorageConfig: {
      name: config.thanos.objectStorage.name,
      key: config.thanos.objectStorage.key,
    },
    serviceMonitor: true,
    volumeClaimTemplate: std.prune(
      pvc.new('data') +
      pvc.metadata.withName(null) +
      pvc.spec.withAccessModes(['ReadWriteOnce']) +
      pvc.spec.withStorageClassName('premium-rwo') +
      pvc.spec.resources.withRequests({
        storage: config.thanos.compact.storage,
      })
    ),
    // Retetion values taken from
    // https://gitlab.com/gitlab-cookbooks/gitlab-prometheus/-/blob/adbbe4c3d03e2873bbdb03530d29be30fbd34824/attributes/thanos.rb#L106-108
    retentionResolutionRaw: '365d',  // 1 year
    retentionResolution5m: '1825d',  // 5 years
    retentionResolution1h: '0d',
  };

  (import 'github.com/thanos-io/kube-thanos/jsonnet/kube-thanos/kube-thanos-compact.libsonnet')(
    staticOverrides +
    config.thanosCompactOverrides
  );

function(params)
  local s = store(params);
  local c = compact(params);

  {
    ['thanos-store-' + name]: s[name]
    for name in std.filter((function(name) name != 'serviceMonitor'), std.objectFields(s))
  } +
  {
    'thanos-store-serviceMonitor': params._addCommonRelabelConfigs(s.serviceMonitor),
  } +
  {
    ['thanos-compact-' + name]: c[name]
    for name in std.filter((function(name) name != 'serviceMonitor'), std.objectFields(c))
  } +
  {
    'thanos-compact-serviceMonitor': params._addCommonRelabelConfigs(c.serviceMonitor),
  }
